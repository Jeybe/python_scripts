# Collatz conjecture

# List to store the collatz conjecture for a specific initial value
collatz_conjecture_values = []

# Calculate every value of the collatz conjecture starting from number and store it
def calculate_collatz_conjecture(number):
	if number > 1:
		if number % 2 == 0:
			collatz_conjecture_values.append(number)
			calculate_collatz_conjecture(number // 2)
		else:
			collatz_conjecture_values.append(number)
			calculate_collatz_conjecture(3 * number + 1)
	else:
		collatz_conjecture_values.append(number)
	return collatz_conjecture_values


if __name__ == '__main__':
	while True:
		try:		
			initial_value = int(input('Please enter a number: '))
			break
		except ValueError:
			print('Error: This wasn\'t a number')
	calculate_collatz_conjecture(initial_value)
	for value in collatz_conjecture_values:
		print(value)
