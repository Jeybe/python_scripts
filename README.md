# python_scripts

Some python scripts I wrote or collected for personal usage or learning purposes.

## Licence

This repository and all files in it are licenced under the [MIT licence](LICENCE).
